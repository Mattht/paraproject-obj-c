//
//  GamePauseLayer.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 11/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GamePauseLayer.h"


@implementation GamePauseLayer
@synthesize isInit, shouldResumeGame;

-(id) initWithScreenSize:(CGSize)_screenSize
{
    
    if( (self=[super init])) {
    
        isInit = false;
        shouldResumeGame = false;
        
        background = [CCSprite spriteWithFile:@"popup.png"];
        [background setPosition:ccp( _screenSize.width / 2, _screenSize.height / 2 )];
        
        [self addChild:background];
        
        // create and initialize a Label
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Pause" fontName:@"Marker Felt" fontSize:50];
        
		// position the label on the center of the screen
		label.position =  ccp( _screenSize.width /2 , _screenSize.height - 100 );
		
		// add the label as a child to this Layer
		[self addChild: label];
        
        
        // Leaderboard Menu Item using blocks
        CCMenuItem *itemResume = [CCMenuItemImage itemWithNormalImage:@"resume.png" selectedImage:@"resume.png" block:^(id sender){
            
            shouldResumeGame = true;
            
        }];
        
		CCMenuItem *itemMainMenu = [CCMenuItemImage itemWithNormalImage:@"quit.png" selectedImage:@"quit.png" block:^(id sender){
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuLayer scene] withColor:ccWHITE]];
            
        }];
		
		CCMenu *menu = [CCMenu menuWithItems:itemResume, itemMainMenu, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		[menu setPosition:ccp( _screenSize.width/2, _screenSize.height/2 - 60)];
		
		// Add the menu to the layer
		[self addChild:menu];
    
    }
    return self;
}

-(void) ResetSettings
{
    isInit = false;
    shouldResumeGame = false;
}

// on "dealloc" you need to release all your retained objects
-(void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
