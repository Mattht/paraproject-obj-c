//
//  GameplayLayer.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//


// Import the interfaces
#import "GameplayLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - GameplayLayer

// HelloWorldLayer implementation
@implementation GameplayLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameplayLayer *layer = [GameplayLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
        // enable touches
		self.isTouchEnabled = YES;
        
        // ask director for the window size
        screenSize = [[CCDirector sharedDirector] winSize];
        
        gamePauseLayer = [[GamePauseLayer alloc] initWithScreenSize:screenSize];
        gameOverLayer = [[GameOverLayer alloc] initWithScreenSize:screenSize];
        
        //landscape = [[CCLayer alloc] init];
        //[landscape setPosition:ccp( screenSize.width / 2, screenSize.height / 2 )];
        //[self addChild:landscape z:0];
        
        landscape = [[Landscape alloc] InitWithScreenSize:screenSize];
        [self addChild:landscape];
        
        //CCSprite *background = [CCSprite spriteWithFile:@"landscape.png"];
        
        //[landscape addChild:background];
        
        lJoystick = [[Joystick alloc] InitAtX: 60 andY: 60 ];
        rJoystick = [[Joystick alloc] InitAtX: screenSize.width - 60 andY: 60 ];
        
		[self addChild:lJoystick z:10];
        [self addChild:rJoystick z:10];
        
        player = [[Player alloc] InitWithScreenSize:screenSize andEnvironment:&landscape ];
        [player LinkMoveJs:&lJoystick andRotateJs:&rJoystick];
		[self addChild:player z:5];
        
        
        pauseButton = [CCSprite spriteWithFile:@"pauseig.png"];
        [pauseButton setPosition:ccp( screenSize.width - 20, screenSize.height - 20 )];
        [self addChild:pauseButton];
        
        //enemy = [[Enemy alloc] InitWithMapSize:screenSize];
        //[enemy LockPlayer:&player];
        //[enemy LinkEnvironment:&landscape];
        //[landscape addChild:enemy z:10];
        
        enemies = [[NSMutableArray alloc] init];
        
        for( int i = 0; i < 10; i++)
        {
            Enemy *enemy;
            enemy = [[Enemy alloc] InitWithScreenSize:screenSize andEnvironment:&landscape ];
            [enemy LockPlayer:&player];
            [landscape addChild:enemy z:10];
            
            [enemies addObject:enemy];
        }
        
        
        hud = [[HUD alloc] InitForScreenSize:screenSize withPlayer:&player];
        [self addChild:hud];
        
        [self schedule: @selector(tick:)];
        
        
	}
	return self;
}

- (void) tick:(ccTime)dt
{
    if( ![hud GameOver] && ![hud GamePaused] )
    {
        [player UpdatePosition];
    
        //[enemy UpdatePosition];
        
        for( Enemy *enemy in enemies )
        {
            [enemy UpdatePosition];
        }
    
        [hud Update];
    }
    else if( [hud GamePaused] )
    {
        if( ![gamePauseLayer isInit] )
        {
            [self addChild:gamePauseLayer z:10];
            [gamePauseLayer setIsInit:true];
        }
        else if( [gamePauseLayer shouldResumeGame] )
        {
            [gamePauseLayer ResetSettings];
            [self removeChild:gamePauseLayer cleanup:NO];
            [hud setGamePaused: false];
        }
    }
    else if( [hud GameOver] )
    {
        if( ![gameOverLayer isInit] )
        {
            [self addChild:gameOverLayer z:10];
            [gameOverLayer DisplayWithScore:[player GetScore]];
            [gameOverLayer setIsInit:true];
        }
    }
    
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSArray *touchArray = [touches allObjects];
    
    for( UITouch *touch in touchArray)
    {
        CGPoint location = [touch locationInView: [touch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        location = [self convertToNodeSpace:location];
        
        if( location.x > screenSize.width - 40 && location.y > screenSize.height - 40 )
        {
            [hud setGamePaused:true];
        }
        else {
            
            if( location.x <= screenSize.width / 2 )
            {
                [lJoystick BeginUpdate:location withHash:[touch hash]];
            }
            else
            {
                [rJoystick BeginUpdate:location withHash:[touch hash]];
            }
            
        }
        
        //CCLOG(@"New: %i", [touch hash] );
    }
}

- (void)ccTouchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    NSArray *touchArray = [touches allObjects];
    
    //CCLOG(@"%i", [touchArray count]);
    
    for( UITouch *touch in touchArray)
    {
        CGPoint location = [touch locationInView: [touch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        location = [self convertToNodeSpace:location];
        
        //CCLOG(@"U: %i", [touch hash] );
        
        if( [lJoystick GetLinkedHash] == [touch hash]  )
        {
            [lJoystick UpdatePosition:location];
            //CCLOG(@"A");
        }
        else if( [rJoystick GetLinkedHash] == [touch hash]  )
        {
            [rJoystick UpdatePosition:location];
            //CCLOG(@"B");
        }
        //CCLOG(@" ------ ");
    }    
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self ccTouchesCancelled:touches withEvent:event];
}

- (void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSArray *touchArray = [touches allObjects];
    
    for( UITouch *touch in touchArray)
    {
        CGPoint location = [touch locationInView: [touch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        location = [self convertToNodeSpace:location];
        
        if( [lJoystick GetLinkedHash] == [touch hash]  )
        {
            [lJoystick ResetPosition];
        }
        else if( [rJoystick GetLinkedHash] == [touch hash]  )
        {
            [rJoystick ResetPosition];
        }
    }
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end