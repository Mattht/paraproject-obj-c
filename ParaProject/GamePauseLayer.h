//
//  GamePauseLayer.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 11/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MainMenuLayer.h"

@interface GamePauseLayer : CCLayer {
    
    CCSprite *background;
    
}
@property bool isInit, shouldResumeGame;

-(id) initWithScreenSize:(CGSize)_screenSize;
-(void) ResetSettings;

@end
