//
//  DataSingleton.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 14/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//


#import "DataSingleton.h"


@implementation DataSingleton

@synthesize ScoreArray;

+(DataSingleton *)getInstance  {
	static DataSingleton *instance;
	
	@synchronized(self) {
		if(!instance) {
			instance = [[DataSingleton alloc] init];
		}
	}
	
	return instance;
}

-(void) SetFromPlist:(NSDictionary*) _dic {
    
    NSLog(@"%@", _dic);

	[self setScoreArray: [_dic objectForKey:@"scores"]];
}

-(NSDictionary*) GetForPlist {
	
	NSDictionary *dic = [NSDictionary dictionaryWithObject:[self ScoreArray] forKey:@"scores" ];
	
	return dic;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	[ScoreArray release];
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
