//
//  Enemy.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Enemy.h"


@implementation Enemy


-(id) InitWithScreenSize:(CGSize)_size andEnvironment:(Landscape**)_environment;
{
    if ((self = [super init]))
    {
        screenSize = _size;
        
        environment = *_environment;
        
        sprite = [CCSprite spriteWithFile:@"Icon-Small-50.png"];
        
        [self Spawn];
        
        
        [self addChild:sprite];
    }
    return self;
}

-(void) Spawn
{
    int rand = arc4random() % 5;
    
    int sideX = 0;
    int sideY = 0;
    
    
    switch ( rand ) {
        case 0:
            sideX =  - 20;
            sideY = arc4random() % (int) [environment GetSize].height;
            break;
        case 1:
            sideX = [environment GetSize].width + 20;
            sideY = arc4random() % (int) [environment GetSize].height;
            break;
        case 2:
            sideX = arc4random() % (int) [environment GetSize].width;
            sideY = - 20;
            break;
        case 3:
            sideX = arc4random() % (int) [environment GetSize].width;
            sideY = [environment GetSize].height + 20;
            break;
    }
    
    sprite.position = ccp( sideX - [environment GetSize].width / 2, sideY - [environment GetSize].width / 2);
    
    int typeRand = ( arc4random() % 2 ) + 1;
    
    deathValue = 10 * typeRand;
    speed = 1 * typeRand;
}

-(void) LockPlayer:(Player**)_player
{
    targetPlayer = *_player;
}

-(CGPoint) GetWorldPosition
{
    worldPosition = ccp( sprite.position.x + 1024 / 2 , sprite.position.y + 1024 / 2 );
    
    return worldPosition;
}

-(void) UpdatePosition
{
    
    // Enenmy rotation:
    
    float deltaY = [targetPlayer GetWorldPosition].y - [self GetWorldPosition].y;
    float deltaX = [targetPlayer GetWorldPosition].x - [self GetWorldPosition].x;
    
    [sprite setRotation: CC_RADIANS_TO_DEGREES( -atan2f(deltaY, deltaX) - M_PI / 2 )];
    
    
    // Enemy position:
    
    CGPoint direction = ccpSub( [targetPlayer GetWorldPosition], [self GetWorldPosition] );
	
    //--- Normalize:
    float length = sqrt( direction.x * direction.x + direction.y * direction.y );
    
    if(length != 0){
        direction.x = direction.x/length;
        direction.y = direction.y/length;
    }    
    [sprite setPosition: ccpAdd( [sprite position], ccpMult( direction, speed ) )];
    
    
    // ------ TEST BULLETS
    
    for( int i = 0; i < [[targetPlayer GetBullets] count]; i++)
    {
        float distance = ccpDistance( [[[targetPlayer GetBullets] objectAtIndex:i] GetWorldPosition], [self GetWorldPosition] );
        
        if ( distance < 20 )
        {
            [targetPlayer IncreaseScore: deathValue];
            [self Spawn];
        }
    }
    
    
    // -------- TEST PLAYER
    
    float distance = ccpDistance( [targetPlayer GetWorldPosition], [self GetWorldPosition] );
    
    if( distance < 25 )
    {
        [targetPlayer RemoveLife];
        [self Spawn];
    }
    
    
    //CCLOG(@"%f,%f", sprite.position.x, sprite.position.y );
    //CCLOG(@"%f,%f", [self GetWorldPosition].x, [self GetWorldPosition].y );
    //CCLOG(@"%f,%f", [[[targetPlayer GetBullets] objectAtIndex:0] GetWorldPosition].x, [[[targetPlayer GetBullets] objectAtIndex:0] GetWorldPosition].y );
}


@end
