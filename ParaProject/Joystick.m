//
//  Joystick.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Joystick.h"


@implementation Joystick
@synthesize offsetX, offsetY, offset, angle;

-(id) InitAtX:(int)_posX andY:(int)_posY
{
    if ((self = [super init]))
    {
        
        position = ccp( _posX, _posY);
        
        [self setOffsetX:0];
        [self setOffsetY:0];
        [self setOffset:0];
        
        bgSprite = [CCSprite spriteWithFile:@"bgJoystick.png"];
    
        jsSprite = [CCSprite spriteWithFile:@"jsJoystick.png"];
        
        bgSprite.position = position;
        jsSprite.position = position;
        
        locked = true;
        
        [self addChild:bgSprite z:1];
        [self addChild:jsSprite z:2];
    
    }
    return self;
    
}

-(unsigned int) GetLinkedHash
{
    return touchHash;
}

-(void) BeginUpdate:(CGPoint)_location withHash:(unsigned int)_hash
{
    locked = false;
    [self UpdatePosition:_location];
    
    touchHash = _hash;
}

-(void) UpdatePosition:(CGPoint)_location
{
    if( !locked )
    {
        float distance = sqrtf( powf( (position.x - _location.x), 2 ) + powf( (position.y - _location.y) , 2 ) );
        
        
        if( distance < 20 )
        {
            jsSprite.position = _location;
        }
        else
        {
            float multiplier = 20 / distance;
            
            CGPoint newPosition = ccpAdd( position, ccpMult( ccpSub( _location, position ), multiplier) );
            
            jsSprite.position = newPosition;
        }
        
        
        [self setOffsetX: jsSprite.position.x - position.x];
        [self setOffsetY: jsSprite.position.y - position.y];
        [self setOffset: sqrtf( powf( ( [self offsetX] ), 2 ) + powf( ( [self offsetY] ) , 2 ) )];
        
        float deltaY = _location.y - position.y;
        float deltaX = _location.x - position.x;
        
        [self setAngle: atan2f(deltaY, deltaX)];
        
        //CCLOG(@"%f / %f", CC_RADIANS_TO_DEGREES([self angle]) , [self angle] );
        
    }
}

-(void) ResetPosition
{
    jsSprite.position = position;
    [self setOffsetX: 0];
    [self setOffsetY: 0];
    [self setOffset: 0];
    touchHash = 0;
    locked = true;
}


@end
