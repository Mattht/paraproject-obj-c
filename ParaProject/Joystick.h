//
//  Joystick.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Joystick : CCNode {
    
    CCSprite *bgSprite;
    CCSprite *jsSprite;
    
    CGPoint position;
    
    bool locked;
   
    unsigned int touchHash;
}
@property float offsetX, offsetY, offset, angle;

-(id) InitAtX:(int)_posX andY:(int)_posY;
-(unsigned int) GetLinkedHash;
-(void) BeginUpdate:(CGPoint)_location withHash:(unsigned int)_hash;
-(void) UpdatePosition:(CGPoint)_location;
-(void) ResetPosition;

@end
