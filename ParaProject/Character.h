//
//  Character.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Landscape.h"

@interface Character : CCNode {
    
    CCSprite *sprite;
    
    CGPoint screenPosition;
    CGPoint worldPosition;
    
    Landscape *environment;
    
    float rotation;
    
    int life;
    float speed;
}

-(id) InitWithScreenSize:(CGSize)_size andEnvironment:(Landscape**)_environment;
-(void) UpdatePosition;

@end
