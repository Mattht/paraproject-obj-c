//
//  DataSingleton.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 14/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DataSingleton : NSObject {
	
    
    NSMutableArray *ScoreArray;
    
}

@property (nonatomic, retain)   NSMutableArray	*ScoreArray;

+(DataSingleton *) getInstance;
-(void) SetFromPlist:(NSDictionary*) _dic;
-(NSDictionary*) GetForPlist;

@end