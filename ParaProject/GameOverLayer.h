//
//  GameOverLayer.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 11/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MainMenuLayer.h"
#import "DataSingleton.h"

@interface GameOverLayer : CCLayer <UITextFieldDelegate>  {
    
    CCSprite *background;
    
    UITextField *nameTextField;
    
    int score;
    
}
@property bool isInit;

-(id) initWithScreenSize:(CGSize)_screenSize;
-(void) saveData;
-(void) DisplayWithScore:(int)_score;
-(void) ReadyForHidding;

@end
