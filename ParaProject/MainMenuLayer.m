//
//  HelloWorldLayer.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "MainMenuLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#import "GameplayLayer.h"
#import "ScoreboardLayer.h"

#pragma mark - MainMenuLayer

// HelloWorldLayer implementation
@implementation MainMenuLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuLayer *layer = [MainMenuLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
		// create and initialize a Label
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Behind Enemy Lines" fontName:@"Marker Felt" fontSize:50];
        
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
		// position the label on the center of the screen
		label.position =  ccp( size.width /2 , size.height - 100 );
		
		// add the label as a child to this Layer
		[self addChild: label];
		
		//
		// Leaderboards and Achievements
		//
		
		// Leaderboard Menu Item using blocks
        CCMenuItem *itemPlay = [CCMenuItemImage itemWithNormalImage:@"Play.png" selectedImage:@"Play.png" block:^(id sender){
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameplayLayer scene] withColor:ccWHITE]];
            
        }];
        
		CCMenuItem *itemScoreboard = [CCMenuItemImage itemWithNormalImage:@"score.png" selectedImage:@"score.png" block:^(id sender){
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[ScoreboardLayer scene] withColor:ccWHITE]];
            
        }];
		
		CCMenu *menu = [CCMenu menuWithItems:itemPlay, itemScoreboard, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		[menu setPosition:ccp( size.width/2, size.height/2 - 60)];
		
		// Add the menu to the layer
		[self addChild:menu];
        
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
