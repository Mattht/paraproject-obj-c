//
//  HUD.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 07/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HUD.h"


@implementation HUD
@synthesize GameOver, GamePaused;

-(id) InitForScreenSize:(CGSize)_size withPlayer:(Player**)_player
{
	if ((self = [super init])) {
        
        [self setGameOver: false];
        [self setGamePaused: false];
        
        targetPlayer = *_player;
        
        lifeLabel = [CCLabelTTF labelWithString:@"3" fontName:@"Marker Felt" fontSize:20];
        [lifeLabel setPosition:ccp( 30, _size.height - 20  )];
        [self addChild:lifeLabel];
        
        scoreLabel = [CCLabelTTF labelWithString:@"0" fontName:@"Marker Felt" fontSize:20];
        [scoreLabel setPosition:ccp( _size.width / 2, _size.height - 20  )];
        [self addChild:scoreLabel];
        
    }
    return self;
}

-(void) Update
{
    [lifeLabel setString:[NSString stringWithFormat:@"Life: %i", [targetPlayer GetLife]]];
    
    score = [targetPlayer GetScore];
    [scoreLabel setString:[NSString stringWithFormat:@"Score: %i", score]];
    
    if( [targetPlayer GetLife] <= 0 )
    {
        
        [self setGameOver:true];
        
        //[self saveData: score];
    }
    
}

- (void)saveData:(int) _score
{
	// get paths from root direcory
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    // get documents path
    NSString *documentsPath = [paths objectAtIndex:0];
    // get the path to our Data/plist file
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Scoreboard.plist"];
	
    NSDictionary *plistDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:_score] forKey:@"1"];
    
    NSString *error = nil;
    // create NSData from dictionary
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:plistDict format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
	
    // check is plistData exists
    if(plistData)
    {
        // write plistData to our Data.plist file
        [plistData writeToFile:plistPath atomically:YES];
    }
    else
    {
        NSLog(@"Error in saveData: %@", error);
        [error release];
    }
}

@end
