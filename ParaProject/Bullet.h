//
//  Bullet.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 05/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Bullet : CCNode {
    
    CCSprite *sprite;
    
    float angle;
    
}
@property float angle;

-(id) InitSprite;
-(CGPoint) GetWorldPosition;
-(void) SetSpritePosition:(CGPoint)_pos;
-(CGPoint) GetSpritePosition;

@end
