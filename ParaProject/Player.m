//
//  Player.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Player.h"


@implementation Player

-(id) InitWithScreenSize:(CGSize)_size andEnvironment:(Landscape**)_environment
{
    if ((self = [super init]))
    {
        screenSize = _size;
        environment = *_environment;
        
        sprite = [CCSprite spriteWithFile:@"robot.png"];
        sprite.position = ccp( screenSize.width / 2, screenSize.height / 2);
    
        bullets = [[NSMutableArray alloc] init];
        for( int i = 0; i < 10; i++ )
        {
            [bullets addObject:[[Bullet alloc] InitSprite]];
        }
        currentBullet = 0;
        fireRateTimer = 0;
        
        life = 3;
        
        for( int i = 0; i < [bullets count]; i++ )
        {
            [environment addChild:[bullets objectAtIndex:i]];
        }
        
        [self addChild:sprite];
    }
    return self;
}

-(void) LinkMoveJs:(Joystick**)_mvJoystick andRotateJs:(Joystick**)_rtJoystick
{
    mvJoystick = *_mvJoystick;
    rtJoystick = *_rtJoystick;
}

-(CGPoint) GetWorldPosition
{
    worldPosition = ccp( - environment.position.x + sprite.position.x + 1024 / 2 , - environment.position.y + sprite.position.y + 1024 / 2 );
    
    return worldPosition;
}

-(void) UpdatePosition
{
    if( [mvJoystick offset] != 0 )
    {
        
        float power = [mvJoystick offset] / 10;
        
        
        environment.position = ccpAdd( environment.position, ccp( cosf( ([mvJoystick angle] + M_PI) ) * power, sinf( ([mvJoystick angle] + M_PI) ) * power ) );
        
        if( environment.position.x > ( 1024 / 2 ) || sprite.position.x < screenSize.width / 2  )
        {
            [environment setPosition: ccp( ( 1024 / 2 ), environment.position.y )];
            
            float newX = min( sprite.position.x - cosf( ([mvJoystick angle] + M_PI) ) * power, screenSize.width / 2 );
            sprite.position = ccp( newX, sprite.position.y );
            
            if( sprite.position.x < 40 )
                sprite.position = ccp( 40, sprite.position.y );
        }
        if( environment.position.x < ( - 1024 / 2 + (screenSize.width / 2) * 2 ) || sprite.position.x > screenSize.width / 2)
        {
            [environment setPosition: ccp( ( - 1024 / 2 + (screenSize.width / 2) * 2 ), environment.position.y )];
            
            float newX = max( sprite.position.x - cosf( ([mvJoystick angle] + M_PI) ) * power, screenSize.width / 2 );
            sprite.position = ccp( newX, sprite.position.y );
            
            if( sprite.position.x > screenSize.width - 40 )
                sprite.position = ccp( screenSize.width - 40, sprite.position.y );
            
        }
        if( environment.position.y > ( 1024 / 2  ) || sprite.position.y < screenSize.height / 2 )
        {
            [environment setPosition: ccp( environment.position.x, ( 1024 / 2  ) )];
            
            float newY = min( sprite.position.y - sinf( ([mvJoystick angle] + M_PI) ) * power , screenSize.height / 2 );
            sprite.position = ccp( sprite.position.x, newY);
            
            if( sprite.position.y < 40 )
                sprite.position = ccp( sprite.position.x, 40);
                
        }
        if( environment.position.y < ( - 1024 / 2 + (screenSize.height / 2) * 2 ) || sprite.position.y > screenSize.height / 2  )
        {
            [environment setPosition: ccp( environment.position.x, ( - 1024 / 2 + (screenSize.height / 2) * 2 ) )];
            
            float newY = max( sprite.position.y - sinf( ([mvJoystick angle] + M_PI) ) * power , screenSize.height / 2 );
            sprite.position = ccp( sprite.position.x, newY);
            
            if( sprite.position.y > screenSize.height - 40 )
                sprite.position = ccp( sprite.position.x, screenSize.height - 40);
        }
         
        /*
        float spriteX = sprite.position.x;
        float spriteY = sprite.position.y;
        
        environment.position = ccpAdd( environment.position, ccp( cosf( ([mvJoystick angle] + M_PI) ) * power, sinf( ([mvJoystick angle] + M_PI) ) * power ) );
        
        if( environment.position.x > ( 1024 / 2 ) || sprite.position.x < screenSize.width / 2  )
        {
            [environment setPosition: ccp( ( 1024 / 2 ), environment.position.y )];
            
            spriteX = max( min( sprite.position.x - cosf( ([mvJoystick angle] + M_PI) ) * power, screenSize.width / 2 ), 40 );
        }
        if( environment.position.x < ( - 1024 / 2 + (screenSize.width / 2) * 2 ) || sprite.position.x > screenSize.width / 2)
        {
            [environment setPosition: ccp( ( - 1024 / 2 + (screenSize.width / 2) * 2 ), environment.position.y )];
            
            spriteX = min( max( sprite.position.x - cosf( ([mvJoystick angle] + M_PI) ) * power, screenSize.width / 2 ), screenSize.width - 40 );
        }
        if( environment.position.y > ( 1024 / 2  ) || sprite.position.y < screenSize.height / 2 )
        {
            [environment setPosition: ccp( environment.position.x, ( 1024 / 2  ) )];
            
            spriteY = max( min( sprite.position.y - sinf( ([mvJoystick angle] + M_PI) ) * power , screenSize.height / 2 ), 40 );
        }
        if( environment.position.y < ( - 1024 / 2 + (screenSize.height / 2) * 2 ) || sprite.position.y > screenSize.height / 2  )
        {
            [environment setPosition: ccp( environment.position.x, ( - 1024 / 2 + (screenSize.height / 2) * 2 ) )];
            
            spriteY = min( max( sprite.position.y - sinf( ([mvJoystick angle] + M_PI) ) * power , screenSize.height / 2 ), screenSize.height - 40 );
        }
        
        sprite.position = ccp( spriteX, spriteY );
        */
        //---
        
        if( [rtJoystick offset] == 0 )
        {
            sprite.rotation = CC_RADIANS_TO_DEGREES( -[mvJoystick angle] - M_PI / 2 );
        }
        
    }

    if( [rtJoystick offset] != 0 )
    {
        sprite.rotation = CC_RADIANS_TO_DEGREES( -[rtJoystick angle] - M_PI / 2 );
        
        if( fireRateTimer > 10 )
        {
            [[bullets objectAtIndex:currentBullet] SetSpritePosition: ccpSub( [self GetWorldPosition], ccp( 1024/2 , 1024/2 ) )];
            [[bullets objectAtIndex:currentBullet] setAngle: [rtJoystick angle] - M_PI  ];
        
            if( currentBullet < [bullets count] - 1 )
                currentBullet++;
            else
                currentBullet = 0;
            
            fireRateTimer = 0;
        }
        else
            fireRateTimer++;
    }
    
   
    for( Bullet *bullet in bullets )
    {
        if( [bullet angle] != 0 )
        {
            [bullet SetSpritePosition: ccpSub( [bullet GetSpritePosition], ccp( cosf( [bullet angle] ) * 5 , sinf( [bullet angle] ) * 5 ) ) ];
            
        }
    }
    
}

-(NSMutableArray*) GetBullets
{
    return bullets;
}

-(void) Fire

{
    
    
}

-(void) ChangeWeapon:(weaponType)_weapon
{
    
}

-(int) GetScore
{
    return score;
}

-(void) AddLife
{
    life++;
}

-(void) RemoveLife
{
    life--;
}

-(int) GetLife
{
    return life;
}

-(void) IncreaseScore:(int)_score
{
    score += _score;
}

-(void) IncreaseSpeed:(int)_speed
{
    
}

-(void) SetShield
{
    
}


@end
