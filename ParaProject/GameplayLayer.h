//
//  GameplayLayer.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Player.h"
#import "Enemy.h"
#import "Joystick.h"
#import "HUD.h"
#import "Landscape.h"
#import "GamePauseLayer.h"
#import "GameOverLayer.h"

// HelloWorldLayer
@interface GameplayLayer : CCLayer
{
    CGSize screenSize;
 
    GamePauseLayer *gamePauseLayer;
    
    GameOverLayer *gameOverLayer;
    
    HUD *hud;
    
    //CCLayer *landscape;
    Landscape *landscape;
    
    Player *player;
    
    CCSprite *pauseButton;
    
    //Enemy *enemy;
    
    NSMutableArray *enemies;
    
    Joystick *lJoystick;
    Joystick *rJoystick;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
