//
//  GameOverLayer.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 11/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameOverLayer.h"


@implementation GameOverLayer
@synthesize isInit;

-(id) initWithScreenSize:(CGSize)_screenSize
{
    
    if( (self=[super init])) {
        
        background = [CCSprite spriteWithFile:@"popup.png"];
        [background setPosition:ccp( _screenSize.width / 2, _screenSize.height / 2 )];
        
        [self addChild:background];
        
        // create and initialize a Label
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Game Over" fontName:@"Marker Felt" fontSize:50];
        
		// position the label on the center of the screen
		label.position =  ccp( _screenSize.width /2 , _screenSize.height - 80 );
		
		// add the label as a child to this Layer
		[self addChild: label];
        
        nameTextField = [[UITextField alloc] initWithFrame: CGRectMake(_screenSize.width / 2 - 90, _screenSize.height / 2 - 35, 180, 30)];
        nameTextField.backgroundColor = [UIColor clearColor];
        nameTextField.borderStyle = UITextBorderStyleNone;
        nameTextField.textAlignment = UITextAlignmentCenter;
        nameTextField.clearsOnBeginEditing = true;
        nameTextField.text = @"YOUR NAME";
        nameTextField.delegate = self;
        nameTextField.returnKeyType = UIReturnKeyDone;
        nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        nameTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        
        CCMenuItem *itemMainMenu = [CCMenuItemImage itemWithNormalImage:@"save.png" selectedImage:@"save.png" block:^(id sender){
            
            // SAVE HERE
            [self saveData];
            
            [self ReadyForHidding];
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuLayer scene] withColor:ccWHITE]];
            
        }];
		
		CCMenu *menu = [CCMenu menuWithItems:itemMainMenu, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		[menu setPosition:ccp( _screenSize.width / 2, _screenSize.height/2 - 60 )];
        
        [self addChild:menu];
    }
    
    return self;
}

-(void) DisplayWithScore:(int)_score
{
    score = _score;
    [[[CCDirector sharedDirector] view] addSubview: nameTextField];
}

-(void) ReadyForHidding
{
    [nameTextField removeFromSuperview];
    
    [nameTextField release];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 10) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    nameTextField.clearsOnBeginEditing = false;
    
    nameTextField.text = [[nameTextField.text componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString: @""];
    
    nameTextField.text = [[nameTextField.text componentsSeparatedByCharactersInSet: [NSCharacterSet punctuationCharacterSet]] componentsJoinedByString: @""];
    
    nameTextField.text = [[nameTextField.text componentsSeparatedByCharactersInSet: [NSCharacterSet symbolCharacterSet]] componentsJoinedByString: @""];
    
    nameTextField.text = [[nameTextField.text componentsSeparatedByCharactersInSet: [NSCharacterSet decimalDigitCharacterSet]] componentsJoinedByString: @""];
    
    nameTextField.text = [nameTextField.text uppercaseString];
    
    [textField resignFirstResponder];
    return YES;
}

- (void)saveData
{
    
    NSMutableArray *scoreArray = [[NSMutableArray alloc] initWithArray:[[DataSingleton getInstance] ScoreArray]];
    
    NSArray *toSave = [[NSArray alloc] initWithObjects:nameTextField.text, [NSNumber numberWithInt:score], nil ];
    
    int i = [scoreArray count] - 1;
    
    while( i > 0 && score > [[[scoreArray objectAtIndex:( i - 1 )] objectAtIndex:1] intValue] )
    {
        [scoreArray replaceObjectAtIndex:i withObject: [scoreArray objectAtIndex:i - 1]];
        
        i--;
    }
    
    [scoreArray replaceObjectAtIndex:i withObject:toSave];
    
    [[DataSingleton getInstance] setScoreArray: scoreArray];
    
    // get paths from root direcory
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    // get documents path
    NSString *documentsPath = [paths objectAtIndex:0];
    // get the path to our Data/plist file
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Scoreboard.plist"];
	
    // create dictionary with values in UITextFields
	//NSDictionary *plistDict = [NSDictionary dictionaryWithObject:today forKey:@"LastPlayed"];
	
	NSDictionary *plistDict = [[DataSingleton getInstance] GetForPlist];
	
    NSString *error = nil;
    // create NSData from dictionary
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:plistDict format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
	
    // check is plistData exists
    if(plistData)
    {
        // write plistData to our Data.plist file
        [plistData writeToFile:plistPath atomically:YES];
    }
    else
    {
        NSLog(@"Error in saveData: %@", error);
        [error release];
    }
}

// on "dealloc" you need to release all your retained objects
-(void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
    
    
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
