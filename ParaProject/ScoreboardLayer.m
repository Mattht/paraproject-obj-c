//
//  ScoreboardLayer.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 10/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ScoreboardLayer.h"


@implementation ScoreboardLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	ScoreboardLayer *layer = [ScoreboardLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
        screenSize = [[CCDirector sharedDirector] winSize];
        
        NSMutableArray *scoreArray = [[NSMutableArray alloc] initWithArray:[[DataSingleton getInstance] ScoreArray] ];
        
        //[self loadData];
		
        int labelY = 20;
        
        for( NSMutableArray *array in scoreArray )
        {
            CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@ - %@", [array objectAtIndex:0], [array objectAtIndex:1] ] fontName:@"Marker Felt" fontSize:20];
            [scoreLabel setPosition:ccp( screenSize.width / 2, screenSize.height - labelY  )];
            
            labelY += 20;
            
            [self addChild:scoreLabel];
        }

        CCMenuItem *itemMainMenu = [CCMenuItemImage itemWithNormalImage:@"quit.png" selectedImage:@"quit.png" block:^(id sender){
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuLayer scene] withColor:ccWHITE]];
            
        }];
		
		CCMenu *menu = [CCMenu menuWithItems:itemMainMenu, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		[menu setPosition:ccp( screenSize.width/2, 60)];
        
        [self addChild:menu];
        
        
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end

