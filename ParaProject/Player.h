//
//  Player.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Character.h"
#import "Bullet.h"
#import "Joystick.h"

typedef enum {
	basic,
	dual,
	spray,
} weaponType;


@interface Player : Character {
    
    CGSize screenSize;
    
    NSMutableArray *bullets;
    int currentBullet;
    int fireRateTimer;
    
    Joystick *mvJoystick;   // Move Joystick
    Joystick *rtJoystick;   // Rotate Joystick
    
    int score;
    
    weaponType weapong;
    
}

-(void) LinkMoveJs:(Joystick**)_mvJoystick andRotateJs:(Joystick**)_rtJoystick;
-(void) Fire;
-(void) ChangeWeapon:(weaponType)_weapon;
-(void) AddLife;
-(void) RemoveLife;
-(int) GetLife;
-(void) IncreaseScore:(int)_score;
-(int) GetScore;
-(void) IncreaseSpeed:(int)_speed;
-(void) SetShield;
-(NSMutableArray*) GetBullets;
-(CGPoint) GetWorldPosition;

@end
