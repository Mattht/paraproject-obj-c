//
//  ScoreboardLayer.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 10/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "MainMenuLayer.h"
#import "DataSingleton.h"

// HelloWorldLayer
@interface ScoreboardLayer : CCLayer
{
    
    CGSize screenSize;
    
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end

