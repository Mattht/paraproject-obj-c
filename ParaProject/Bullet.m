//
//  Bullet.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 05/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Bullet.h"


@implementation Bullet
@synthesize angle;

-(id) InitSprite
{
	if ((self = [super init])) {
        
        sprite = [CCSprite spriteWithFile:@"bullet.png"];
        sprite.position = ccp( 1024, 1024 );
        
        [self addChild:sprite];
        
        angle = 0;
        
    }
    return self;
}

-(CGPoint) GetWorldPosition
{
    return ccp( sprite.position.x + 1024 / 2 , sprite.position.y + 1024 / 2 );
}

-(void) SetSpritePosition:(CGPoint)_pos
{
    sprite.position = _pos;
}

-(CGPoint) GetSpritePosition
{
    return sprite.position;
}

@end
