//
//  HUD.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 07/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Player.h"

@interface HUD : CCNode {
    
    Player *targetPlayer;
    
    int score;
    
    CCLabelTTF *lifeLabel;
    CCLabelTTF *scoreLabel;
    
}
@property bool GameOver, GamePaused;

-(id) InitForScreenSize:(CGSize)_size withPlayer:(Player**)_player;
-(void) Update;

@end
