//
//  Landscape.m
//  ParaProject
//
//  Created by MATTHEW SELLERS on 14/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Landscape.h"


@implementation Landscape

-(id) InitWithScreenSize:(CGSize)_screenSize
{
	if ((self = [super init])) {
        
        mSprite = [CCSprite spriteWithFile:@"landscape.png"];
        
        [self setPosition:ccp( _screenSize.width / 2, _screenSize.height / 2 )];
        
        [self addChild:mSprite];
        
    }
    return self;
}

-(CGSize) GetSize
{
    return [mSprite boundingBox].size;
}

@end
