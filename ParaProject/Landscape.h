//
//  Landscape.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 14/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Landscape : CCNode {
    
    CCSprite *mSprite;
    
}

-(id) InitWithScreenSize:(CGSize)_screenSize;
-(CGSize) GetSize;

@end