//
//  Enemy.h
//  ParaProject
//
//  Created by MATTHEW SELLERS on 02/12/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Character.h"
#import "Player.h"

@interface Enemy : Character {
    
    Player *targetPlayer;
    
    CGSize screenSize;
    
    int deathValue;
}

-(void) LockPlayer:(Player**)_player;
-(CGPoint) GetWorldPosition;

@end
